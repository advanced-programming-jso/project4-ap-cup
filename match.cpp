#include "match.h"

Match_class::Match_class() {
	
};

void Match_class::set_teams(Team a, Team b) {
	t1 = a;
	t2 = b;
};

Result Match_class::play_match() {
	Result r;
	Match m1 = win_calc(t1, t2);
	Match m2 = win_calc(t2, t1);
	r.first_game = m1;
	r.second_game = m2;
	if(m1.goals_home + m2.goals_away > m2.goals_home + m1.goals_away) {
		r.winner = m1.team_home;
		r.looser = m2.team_home;
	} else if(m1.goals_home + m2.goals_away < m2.goals_home + m1.goals_away) {
		r.winner = m2.team_home;
		r.looser = m1.team_home;
	} else {
		if(win_penalty(t2, t1) == 1){
			r.winner = m1.team_home;
			r.looser = m2.team_home;
		} else if(win_penalty(t2, t1) == 2) {
			r.winner = m2.team_home;
			r.looser = m1.team_home;
		}
	}

	return r;
};

int win_penalty(Team t1, Team t2) {
	Power_class p_class1(t1);
	Power_class p_class2(t2);
	Power pow_t1 = p_class1.pow_calc();
	Power pow_t2 = p_class2.pow_calc();
	Fans_impact fans_impact_t1 = t1.stadium.impact * (t1.fans_in_stadium[t1.stadium.stadium_name] / t1.stadium.stadium_capacity);
	Fans_impact fans_impact_t2;
	if(t1.stadium.stadium_capacity - t1.fans_in_stadium[t1.stadium.stadium_name] - t2.fans_in_stadium[t1.stadium.stadium_name] >= 0){
		fans_impact_t2 = t1.stadium.impact * (t2.fans_in_stadium[t1.stadium.stadium_name] / t1.stadium.stadium_capacity);
	} else {
		fans_impact_t2 = t1.stadium.impact * ((t1.stadium.stadium_capacity - t1.fans_in_stadium[t1.stadium.stadium_name]) / t1.stadium.stadium_capacity);
	}

	pow_t1.attack += fans_impact_t1;
	pow_t2.attack += fans_impact_t2;

	double win_prob1 = pow_t1.attack / pow_t2.goalkeeper;
	double win_prob2 = pow_t2.attack / pow_t1.goalkeeper;

	if(win_prob1 > win_prob2)
		return 1;
	else 
		return 2;
};

Match win_calc(Team t1, Team t2) {
	Power_class p_class1(t1);
	Power_class p_class2(t2);
	Power pow_t1 = p_class1.pow_calc();
	Power pow_t2 = p_class2.pow_calc();
	Fans_impact fans_impact_t1 = t1.stadium.impact * ((double)t1.fans_in_stadium[t1.stadium.stadium_name] / t1.stadium.stadium_capacity);
	Fans_impact fans_impact_t2;
	if(t1.stadium.stadium_capacity - t1.fans_in_stadium[t1.stadium.stadium_name] - t2.fans_in_stadium[t1.stadium.stadium_name] >= 0){
		fans_impact_t2 = t1.stadium.impact * ((double)t2.fans_in_stadium[t1.stadium.stadium_name] / t1.stadium.stadium_capacity);
	} else {
		fans_impact_t2 = t1.stadium.impact * ((double)(t1.stadium.stadium_capacity - t1.fans_in_stadium[t1.stadium.stadium_name]) / t1.stadium.stadium_capacity);
	}

	pow_t1.attack += fans_impact_t1;pow_t1.defend += fans_impact_t1;pow_t1.midfield += fans_impact_t1;
	pow_t2.attack += fans_impact_t2;pow_t2.defend += fans_impact_t2;pow_t1.midfield += fans_impact_t2;

	Match m;
	m.team_home = t1.team_name;
	m.team_away = t2.team_name;
	m.goals_home = pow((pow_t1.attack / pow_t2.defend) * (pow_t1.midfield / pow_t2.midfield), 1.5) * 3;
	m.goals_away = pow((pow_t2.attack / pow_t1.defend) * (pow_t2.midfield / pow_t1.midfield), 1.5) * 3;

	return m;
};