#include "printer.h"

Printer::Printer(Results r) {
	results = r;
}

void Printer::print_round_results(int round) {
	cout << "Round " << round << endl;
	for(int i = 0; i < results[round-1].size(); i++) {
		cout << i+1 << ':' << endl;
		cout << '\t' << results[round-1][i].first_game.team_home << ' ' << results[round-1][i].first_game.goals_home <<
				" - " << results[round-1][i].first_game.goals_away << ' ' << results[round-1][i].first_game.team_away << endl;
		cout << '\t' << results[round-1][i].second_game.team_home << ' ' << results[round-1][i].second_game.goals_home <<
				" - " << results[round-1][i].second_game.goals_away << ' ' << results[round-1][i].second_game.team_away << endl;			
		cout << "\twinner: " << results[round-1][i].winner << endl;
	}

};
void Printer::print_tournament_results() {
	for(int i = 0; i < results.size(); i++)
		print_round_results(i+1);
};
void Printer::print_team_results(string team_name) {
	for(int i = 0; i < results.size(); i++) {
		for(int j = 0; j < results[i].size(); j++) {
			if(results[i][j].winner == team_name || results[i][j].looser == team_name) {
				cout << "Round " << i+1 << endl;
				cout << i+1 << ':' << endl;
				cout << '\t' << results[i][j].first_game.team_home << ' ' << results[i][j].first_game.goals_home <<
							" - " << results[i][j].first_game.goals_away << ' ' << results[i][j].first_game.team_away << endl;
				cout << '\t' << results[i][j].second_game.team_home << ' ' << results[i][j].second_game.goals_home <<
							" - " << results[i][j].second_game.goals_away << ' ' << results[i][j].second_game.team_away << endl;			
				cout << "\twinner: " << results[i][j].winner << endl;
			}
		}
	}
};