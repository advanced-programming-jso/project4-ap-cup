#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <map>

using namespace std;

#define GOALKEEPER "goalkeeper"
#define DEFENDER "defener"
#define MIDFIELDER "midfielder"
#define STRIKER "striker"

#define GK 0
#define DF 1
#define MF 2
#define ST 3

#define GOALKEEPERMIN 1
#define DEFENDERMIN 3
#define MIDFIELDERMIN 3
#define STRIKERMIN 2

struct Power {
	double attack;
	double defend;
	double midfield;
	double goalkeeper;
};

struct Stadium {
	string stadium_name;
	int stadium_capacity;
	float impact;
};
typedef vector<Stadium> Stadiums;

typedef map<string, int> Fans_in_stadium;

struct Player {
	string player_name;
	double player_power;
	string player_post;
};
typedef vector<Player> Players;
typedef vector<Players> Players_per_post;

struct Team {
	string team_name;
	Stadium stadium;
	Fans_in_stadium fans_in_stadium;
	Players_per_post players_per_post;
};
typedef vector<Team> Teams;

struct Match {
	string team_home;
	string team_away;
	int goals_home;
	int goals_away;
};

struct Result {
	Match first_game;
	Match second_game;
	string winner;
	string looser;
};
typedef vector<Result> Row_results;
typedef vector<Row_results> Results;

typedef double Fans_impact;