#include "power.h"

Power_class::Power_class(Team t) {
	team = t;
};

Power Power_class::pow_calc() {
	Power p;
	p.attack = floor((pow_attakers(team.players_per_post[ST]) / team.players_per_post[ST].size()) * pow(((double)team.players_per_post[ST].size() / 3), 0.5));
	p.midfield = floor((pow_midfielders(team.players_per_post[MF]) / team.players_per_post[MF].size()) * pow(((double)team.players_per_post[MF].size() / 3), 0.5));
 	p.defend =floor(((pow_defenders(team.players_per_post[DF]) + team.players_per_post[GK][0].player_power) / (team.players_per_post[DF].size() + 1)) * pow(((double)(team.players_per_post[DF].size()) / 4), 0.5));
 	p.goalkeeper = team.players_per_post[GK][0].player_power;
 	return p;
};

double pow_attakers(Players p){
	double n = 0;
	for(int i = 0; i < p.size(); i++) {
		n += p[i].player_power;
	}
	return n;
};

double pow_defenders(Players p) {
	double n = 0;
	for(int i = 0; i < p.size(); i++) {
		n += p[i].player_power;
	}
	return n;
};

double pow_midfielders(Players p) {
	double n = 0;
	for(int i = 0; i < p.size(); i++) {
		n += p[i].player_power;
	}
	return n;
};