#include "match.h"

class Tournament;
class Handler {
public:
	Handler(Teams t, Stadiums s);
	Handler(Results r);

	void set_teams(Teams t);
	void set_stadiums(Stadiums s);
	void set_results(Results r);

	void simulate(int round);
	void teams_manager(int round);
	Team winner_team(string team_name);
	int error_handler();
	void best_eleven();

	Results get_results();
private:
	Teams teams;
	Stadiums stadiums;
	Results results;
};

int error_we_are_eleven(Teams teams);
int error_players_num_posts(Teams teams);
bool error_not_pow2(Teams teams);
bool goalkeeper_num(Players_per_post &players_per_post);
bool defender_num(Players_per_post &players_per_post);
bool midfielder_num(Players_per_post &players_per_post);
bool striker_num(Players_per_post &players_per_post);
bool compare_by_power(const Player &a, const Player &b);
bool compare_by_name(const Team &a, const Team &b);
int find_post_id(Player p);