#declare the variable
CC=g++

CFLAGS=-c

all: core

core: printer.o power.o match.o handler.o tournament.o main.o
	  	$(CC) printer.o power.o match.o handler.o tournament.o main.o -o output

main.o: main.cpp
		$(CC) $(CFLAGS) main.cpp

tournament.o: tournament.h tournament.cpp 
		$(CC) $(CFLAGS) tournament.h tournament.cpp

handler.o: handler.h handler.cpp
		$(CC) $(CFLAGS) handler.h handler.cpp

match.o: match.h match.cpp
		$(CC) $(CFLAGS) match.h match.cpp

power.o: power.h power.cpp
		$(CC) $(CFLAGS) power.h power.cpp

printer.o: printer.h printer.cpp
		$(CC) $(CFLAGS) printer.h printer.cpp

clean:
		rm -rf *o core && rm -rf *gch core