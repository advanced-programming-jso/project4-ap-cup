#include "power.h"

class Match_class {
public:
	Match_class();
	void set_teams(Team a, Team b);
	Result play_match();
private:
	Match match;
	Team t1, t2;
};

Match win_calc(Team t1, Team t2);
int win_penalty(Team t1, Team t2);