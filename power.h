#include "printer.h"

class Power_class {
public:
	Power_class(Team t);
	Power pow_calc();
private:
	double attack;
	double defend;
	double midfield;
	double goalkeeper;
	Team team;
};

double pow_attakers(Players p);
double pow_defenders(Players p);
double pow_midfielders(Players p);