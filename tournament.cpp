#include "tournament.h"

Tournament::Tournament() {

};

void Tournament::add_team(string team_name, string stadium_name, int capacity,
					float impact, Fans_in_stadium fans_in_stadium) {
	int team_id = find_team_id(teams, team_name);
	int stadium_id = find_stadium_id(stadiums, stadium_name);
	if(team_id != -1){
		cout << "there is a team <" << team_name << ">. please submit another team or add player." << endl;
		return;
	}
	if(stadium_id != -1){
		cout << "there is a stadium <" << stadium_name << ">. and it is for another team please enter your teams stadium name." << endl;
		return;
	}

	Stadium stadium;
	stadium.stadium_name = stadium_name;
	stadium.stadium_capacity = capacity;
	stadium.impact = impact;
	stadiums.push_back(stadium);
	
	Players_per_post players_per_post(4);
	
	Team team;
	team.team_name = team_name;
	team.stadium = stadium;
	team.fans_in_stadium = fans_in_stadium;
	team.players_per_post = players_per_post;


	teams.push_back(team);
};

void Tournament::add_player(string team_name, string player_name, int player_power,
					string player_post) {
	int team_id = find_team_id(teams, team_name);
	if(team_id == -1) {
		cout << "there is no team with name : " << team_name << " check data again." << endl;
		return;
	}
	Player player;
	player.player_name = player_name;
	player.player_power = player_power;
	player.player_post = player_post;

	int post = find_post_id(player_post);

	teams[team_id].players_per_post[post].push_back(player);
};

void Tournament::simulate() {
	Handler h(teams, stadiums);
	int error = h.error_handler();
	if(error == -2) {
		cout << "Inadequate teams" << endl;
		return;
	} else if(error != -1){
		cout << "Inadequate players in " << teams[error].team_name << endl;
		return;
	}
	h.simulate(0);
	results = h.get_results();
};


void Tournament::print_round_results(int round_number) {
	Printer p(results);
	p.print_round_results(round_number);
};
void Tournament::print_tournament_results() {
	Printer p(results);
	p.print_tournament_results();
};
void Tournament::print_team_results(string team_name) {
	Printer p(results);
	p.print_team_results(team_name);
};

void Tournament::print() {
	for(int i = 0; i < teams.size(); i++) {
		cout << teams[i].team_name << "---" << teams[i].stadium.stadium_name << "---" << teams[i].players_per_post[MF][0].player_name << endl;
	}
};

int find_team_id(Teams teams, string team_name) {
	for(int i = 0; i < teams.size(); i++)
		if(teams[i].team_name == team_name)
			return i;
	return -1;
};

int find_stadium_id(Stadiums stadiums, string stadium_name) {
	for(int i = 0; i < stadiums.size(); i++)
		if(stadiums[i].stadium_name == stadium_name)
			return i;
	return -1;
};

int find_post_id(string post_name) {
	if(post_name == GOALKEEPER)
		return GK;
	else if(post_name == DEFENDER)
		return DF;
	else if(post_name == MIDFIELDER)
		return MF;
	else 
		return ST;
};
