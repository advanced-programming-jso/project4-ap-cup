#include "handler.h"

Handler::Handler(Teams t, Stadiums s) {
	set_teams(t);
	set_stadiums(s);
	best_eleven();
};

void Handler::best_eleven() {
	for (int i = 0; i < teams.size(); i++) {
		sort(teams[i].players_per_post[GK].begin(), teams[i].players_per_post[GK].end(), compare_by_power);
		sort(teams[i].players_per_post[DF].begin(), teams[i].players_per_post[DF].end(), compare_by_power);
		sort(teams[i].players_per_post[MF].begin(), teams[i].players_per_post[MF].end(), compare_by_power);
		sort(teams[i].players_per_post[ST].begin(), teams[i].players_per_post[ST].end(), compare_by_power);
		Players p;
		if(teams[i].players_per_post[GK].size() > 1)
			for(int j = 1; j < teams[i].players_per_post[GK].size(); j++) {
				teams[i].players_per_post[GK].pop_back();
			}
		if(teams[i].players_per_post[DF].size() > 1)
			for(int j = teams[i].players_per_post[DF].size() - 1; j > DEFENDERMIN - 1; j--) {
				p.push_back(teams[i].players_per_post[DF][j]);
				teams[i].players_per_post[DF].pop_back();
			}
		if(teams[i].players_per_post[MF].size() > 1)
			for(int j = teams[i].players_per_post[MF].size() - 1; j > MIDFIELDERMIN - 1; j--) {
				p.push_back(teams[i].players_per_post[MF][j]);
				teams[i].players_per_post[MF].pop_back();
			}
		if(teams[i].players_per_post[ST].size() > 1)
			for(int j = teams[i].players_per_post[ST].size() - 1; j > STRIKERMIN - 1; j--) {
				p.push_back(teams[i].players_per_post[ST][j]);
				teams[i].players_per_post[ST].pop_back();
			}
		sort(p.begin(), p.end(), compare_by_power);
		teams[i].players_per_post[find_post_id(p[0])].push_back(p[0]);
		teams[i].players_per_post[find_post_id(p[1])].push_back(p[1]);
	}
};

int find_post_id(Player p) {
	if(p.player_post == GOALKEEPER)
		return GK;
	else if(p.player_post == DEFENDER)
		return DF;
	else if(p.player_post == MIDFIELDER)
		return MF;
	else if(p.player_post == STRIKER)
		return ST;
};

Handler::Handler(Results r) {
	set_results(r);
};

void Handler::set_teams(Teams t) {
	teams = t;
};

void Handler::set_stadiums(Stadiums s) {
	stadiums = s;
};

void Handler::set_results(Results r) {
	results = r;
};

Results Handler::get_results() {
	return results;
}

void Handler::simulate(int round) {
	sort(teams.begin(), teams.end(), compare_by_name);
	if(teams.size() == 1)
		return;
	Row_results r;
	Match_class m;
	for (int i = 0; i < teams.size(); i+=2) {
		m.set_teams(teams[i], teams[i+1]);
		r.push_back(m.play_match());
	}
	results.push_back(r);
	teams_manager(round);
	simulate(round + 1);
};

void Handler::teams_manager(int round) {
	Teams t;
	for(int i = 0; i < results[round].size(); i++){
		t.push_back(winner_team(results[round][i].winner));
	}
	teams.clear();
	teams = t;
};

Team Handler::winner_team(string team_name) {
	for(int i = 0; i < teams.size(); i++) {
		if(teams[i].team_name == team_name)
			return teams[i];
	}
};

bool compare_by_name(const Team &a, const Team &b) {
	return a.team_name < b.team_name;
};

int Handler::error_handler() {
	int error_eleven = error_we_are_eleven(teams);
	int error_posts = error_players_num_posts(teams);
	bool error_pow2 = error_not_pow2(teams);
	if(error_eleven != -1)
		return error_eleven;
	else if(error_posts != -1)
		return error_posts;
	else if(!error_pow2)
		return -2;
	else
		return -1;
};

int error_we_are_eleven(Teams teams) {
	for(int i = 0; i < teams.size(); i++) {
		int n = 0;
		for(int j = 0; j < teams[i].players_per_post.size(); j++)
			n += teams[i].players_per_post[j].size();
		if(n < 11)
			return i;
	}
	return -1;
};

bool error_not_pow2(Teams teams) {
	return (teams.size() & (teams.size() - 1)) == 0;
};

int error_players_num_posts(Teams teams) {
	for(int i = 0; i < teams.size(); i++) {
		if(!goalkeeper_num(teams[i].players_per_post))
			return i;
		else if(!defender_num(teams[i].players_per_post))
			return i;
		else if(!midfielder_num(teams[i].players_per_post))
			return i;
		else if(!striker_num(teams[i].players_per_post))
			return i;
	}
	return -1;
};

bool goalkeeper_num(Players_per_post &players_per_post) {
	if(players_per_post[GK].size() >= GOALKEEPERMIN){
		sort(players_per_post[GK].begin(), players_per_post[GK].end(), compare_by_power);
		return true;
	}
	return false;
};

bool defender_num(Players_per_post &players_per_post) {
	if(players_per_post[DF].size() >= DEFENDERMIN){
		sort(players_per_post[DF].begin(), players_per_post[DF].end(), compare_by_power);
		return true;
	}
	return false;
};

bool midfielder_num(Players_per_post &players_per_post) {
	if(players_per_post[MF].size() >= MIDFIELDERMIN){
		sort(players_per_post[MF].begin(), players_per_post[MF].end(), compare_by_power);
		return true;
	}
	return false;
};

bool striker_num(Players_per_post &players_per_post) {
	if(players_per_post[ST].size() >= STRIKERMIN){
		sort(players_per_post[ST].begin(), players_per_post[ST].end(), compare_by_power);
		return true;
	}
	return false;
};

bool compare_by_power(const Player &a, const Player &b) {
    return a.player_power > b.player_power;
}