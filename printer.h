#include "lib.h"

class Printer {
public:
	Printer(Results r);

	void print_round_results(int round);
	void print_tournament_results();
	void print_team_results(string team_name);
private:
	Results results;
};