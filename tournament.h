#include "handler.h"

class Tournament {
public:
	Tournament();
	void add_team(string team_name, string stadium_name, int capacity,
					float impact, Fans_in_stadium fans_in_stadium);
	void add_player(string team_name, string player_name, int player_power,
					string player_post);
	void simulate();

	void print_round_results(int round_number);
	void print_tournament_results();
	void print_team_results(string team_name);

	void print();

private:
	Teams teams;
	Stadiums stadiums;
	Results results;
};

int find_team_id(Teams teams, string team_name);
int find_stadium_id(Stadiums stadiums, string stadium_name);
int find_post_id(string post_name);